import datetime, locale
from django.shortcuts import render
from django.urls import reverse_lazy, reverse

from django.views.generic import TemplateView, ListView

from django.contrib.auth.mixins import LoginRequiredMixin

from ..libro.models import Libro


class HomePageView(ListView):
    context_object_name = 'top_sellers'
    template_name = 'home/index.html'
    login_url = reverse_lazy('users_app:user-login')

    def get_queryset(self):
        return Libro.objects.listarLibrosTop()