from django.contrib import admin
from django.urls import path

from . import views

app_name = 'libros_app'

urlpatterns = [
    path('libros/', views.ListLibros.as_view(), name='libros'),
    path('librosCategoria/<categoria>/', views.ListLibrosCategoria.as_view(), name='libros-categoria'),
    path('libroDetalle/<pk>/', views.LibroDetailView.as_view(), name='libro-detalle'),
    path('categorias/', views.CategoriaListView.as_view(), name='categorias'),
]
