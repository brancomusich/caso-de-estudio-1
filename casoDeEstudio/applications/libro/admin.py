from django.contrib import admin

# Register your models here.
from .models import Libro, Categoria


class LibroAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'titulo',
        'categoria',
        'fecha',
        'inventario',
    )
    search_fields = ('titulo',)
    list_filter = ('categoria',)

admin.site.register(Libro, LibroAdmin)

admin.site.register(Categoria)