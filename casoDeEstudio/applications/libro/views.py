from django.shortcuts import render
from django.views.generic import ListView, DetailView
# Create your views here.
from .models import Libro, Categoria
# Create your views here.
class ListLibros(ListView):

    context_object_name = 'libros'
    paginate_by = 6
    template_name = 'libro/lista.html'

    def get_queryset(self):
        palabraClave = self.request.GET.get('nombre', '')
        return Libro.objects.buscarLibro(palabraClave)

class ListLibrosCategoria(ListView):

    context_object_name = 'libros'
    template_name = 'libro/lista_categoria.html'

    def get_queryset(self):
        categoria = self.kwargs['categoria']
        palabraClave = self.request.GET.get('nombre', '')
        return Libro.objects.listarLibrosCategoria(categoria, palabraClave)


class LibroDetailView(DetailView):
    model = Libro
    template_name = 'libro/detalle.html'

    def get_context_data(self, **kwargs):
        context = super(LibroDetailView, self).get_context_data(**kwargs)
        libroC = Libro.objects.get(pk=self.kwargs['pk'])
        context['recomendados'] = Libro.objects.listarLibrosRecomendados(libroC.categoria.id, libroC.id)
        return  context

class CategoriaListView(ListView):
    model = Categoria
    context_object_name = 'categorias'
    template_name = 'libro/categorias.html'
    paginate_by = 10
