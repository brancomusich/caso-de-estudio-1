from django.db import models

# Create your models here.
from django.db.models.signals import post_save
from PIL import Image

from .managers import CategoriaManager, LibroManager


class Categoria(models.Model):
    nombre = models.CharField('Nombre', max_length=30)

    objects = CategoriaManager()

    def __str__(self):
        return str(self.id) + '-' + self.nombre
    class Meta:
        verbose_name = 'Categoria'
        verbose_name_plural = 'Categorias'

class Libro(models.Model):
    categoria = models.ForeignKey(Categoria, on_delete=models.CASCADE, verbose_name='Categoría', related_name='categoria_libro')
    autor = models.CharField(verbose_name='Autores',max_length=50)
    titulo = models.CharField('Titulo', max_length=50)
    fecha = models.DateField('Fecha de lanzamiento', auto_now=False, auto_now_add=False)
    portada = models.ImageField('Portada', upload_to='libro', blank=True, null=True)
    precio = models.PositiveIntegerField('Precio')
    inventario = models.PositiveIntegerField(default=10)

    objects = LibroManager()
    def __str__(self):
        return self.titulo

    class Meta:
        verbose_name = 'Libro'
        verbose_name_plural = 'Libros'
        ordering = ['titulo', 'fecha']

def optimize_image(sender, instance, **kargs):
    if(instance.portada):
        portada = Image.open(instance.portada.path)
        #con esto optimizamos y reducimos un 20% la calidad
        portada.save(instance.portada.path, quality = 20, optimize = True)
post_save.connect(optimize_image,sender= Libro)