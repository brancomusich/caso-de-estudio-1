import re
from django.db import models

from django.db.models import Avg, Sum, Count
from django.db.models.functions import Lower
from django.contrib.postgres.search import TrigramSimilarity


class LibroManager(models.Manager):
    """Managers para el modelo autor"""

    def listarLibros(self):
       return self.all()

    def listarLibrosTop(self):
        # annotate se guia en base del pk del modelo por lo que gfue más facil
        # realizar el conteo de copias prestadas de un libro desde este modelo
        # usando el related_name
        resultado = self.annotate(
            numPrestados=Count('libro_prestamo'),
            cantidad= Sum('libro_prestamo__cantidad'),
        ).order_by('-cantidad').exclude(cantidad =None)
        return resultado[:3]


    def buscarLibro(self, valor):
        if(valor == None):
            resultado = self.all()[:10]
        elif re.match(r"^([A-Za-záéíóúÁÉÍÓÚñ]+\s+[A-Za-záéíóúÁÉÍÓÚñ]+((\s+[A-Za-záéíóúÁÉÍÓÚñ]+)+)?)$", valor):
            print('paso')
            resultado = self.filter(
                titulo__trigram_similar=valor
            )
        else:
            resultado = self.filter(
                titulo__icontains=valor
            )
        return resultado

    def listarLibrosCategoria(self, categoria, titulo):
        if re.match(r"^([A-Za-záéíóúÁÉÍÓÚñ]+\s+[A-Za-záéíóúÁÉÍÓÚñ]+((\s+[A-Za-záéíóúÁÉÍÓÚñ]+)+)?)$", titulo):
            print('paso')
            resultado = self.filter(
                categoria__id = categoria
            ).filter(
                titulo__trigram_similar = titulo
            ).order_by('titulo')
        else:
            resultado = self.filter(
                categoria__id = categoria
            ).filter(
                titulo__icontains = titulo
            ).order_by('titulo')

        return resultado

    def listarLibrosRecomendados(self, categoria, libro):
        resultado = self.filter(
            categoria__id = categoria
        ).exclude(
            id = libro
        ).order_by('titulo')[:10]
        return resultado

    def librosNumVentas(self):
        #devuelve un diccionario de python aggregate
        resultado = self.aggregate(
            cantVentas = Count('libro_venta')
        )
        return resultado

    def numLibrosVendidos(self):
        #annotate se guia en base del pk del modelo por lo que gfue más facil
        #realizar el conteo de copias vendidas de un libro desde este modelo
        #usando el related_name
        resultado = self.annotate(
            numVendidos = Count('libro_venta')
        )
        for r in resultado:
            print(r, r.numVendidos)
        return resultado

class CategoriaManager(models.Manager):

    def listarCategoriaLibros(self):
        #es parecido a un groupBy de sql y agrega una columna al queryset
        resultado = self.annotate(
            cantidadLibros=Count('categoria_libro')
        )
        for r in resultado:
            print(r, r.cantidadLibros)
        return resultado
