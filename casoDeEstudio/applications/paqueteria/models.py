from django.db import models

# Create your models here.
class Paqueteria(models.Model):
    nombre = models.CharField('Paqueteria', max_length=50)
    costo = models.PositiveIntegerField('Costo de envio', default=99)

    #objects = PaqueteriaManager()

    def __str__(self):
        return self.nombre + ' (costo $ ' + str(self.costo) + ')'

    class Meta:
        verbose_name = 'Paqueteria'
        verbose_name_plural = 'Paqueterias'
        ordering = ['nombre',]
