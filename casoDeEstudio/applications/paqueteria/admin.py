from django.contrib import admin

# Register your models here.
from .models import Paqueteria


class PaqueteriaAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'nombre',
        'costo',
    )
admin.site.register(Paqueteria, PaqueteriaAdmin)