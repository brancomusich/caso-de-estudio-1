from django.apps import AppConfig


class PaqueteriaConfig(AppConfig):
    name = 'paqueteria'
