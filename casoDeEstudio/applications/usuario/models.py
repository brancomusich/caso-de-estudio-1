from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
#managers propios
from .managers import UserManager, OrdenManager
# Create your models here.
#este permisionMixin sustituira el users por default de django
from ..libro.models import Libro
from ..paqueteria.models import Paqueteria


class User(AbstractBaseUser, PermissionsMixin):

    username = models.CharField('Nombre de usuario', max_length=10, unique=True)
    email = models.EmailField('Email', unique=True)
    nombre = models.CharField('Nombre(s)', max_length=30, blank=True)
    apellido = models.CharField('Apellido(s)', max_length=30, blank=True)
    codregistro = models.CharField(max_length=6, blank=True)
    avatar = models.ImageField('Avatar',upload_to='usuario',null=True, blank=True)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)

    USERNAME_FIELD = 'username'

    REQUIRED_FIELDS = ['email',]

    objects = UserManager()

    def get_short_name(self):
        return self.username

    def get_full_name(self):
        return self.username + ' ' + self.apellido

class Orden(models.Model):
    usuario = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='Cliente')
    libro = models.ForeignKey(Libro, on_delete=models.CASCADE, verbose_name='Libro', related_name='libro_prestamo')
    paqueteria = models.ForeignKey(Paqueteria, on_delete=models.CASCADE, verbose_name='Paqueteria', related_name='libro_paqueteria')
    fecha_venta = models.DateField('Fecha de la venta')
    fecha_entrega = models.DateField('Fecha de la entrega', blank=True, null=True)
    cantidad = models.PositiveIntegerField('Cantidad', default=1)
    total = models.PositiveIntegerField('Total',blank=True,null=True)
    domicilio = models.CharField('Domicilio', max_length=60)

    objects = OrdenManager()
    #haciendo esto, cada vez que se guarde desde una vista o el admin
    #se ejecutara
    def save(self, *args, **kwargs):
        self.total = (self.libro.precio * self.cantidad) + self.paqueteria.costo
        self.libro.inventario -= self.cantidad
        self.libro.save()
        return super(Orden,self).save(*args, **kwargs)

    def __str__(self):
        return str(self.id) + '-orden de ' + self.usuario.nombre

    class Meta:
        verbose_name = 'Orden'
        verbose_name_plural = 'Ordenes'

