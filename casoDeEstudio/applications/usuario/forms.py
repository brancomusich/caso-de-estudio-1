import re
from django import forms
from django.contrib.auth import authenticate
from .models import User, Orden
from django.core.exceptions import ValidationError

from ..libro.models import Libro


class UserRegisterForm(forms.ModelForm):
    password = forms.CharField(label='Contraseña',
                               required=True,
                               widget=forms.PasswordInput(
                                   attrs={
                                       'placeholder': 'Contraseña'
                                   }
                               ))
    password2 = forms.CharField(label='Contraseña',
                                required=True,
                                widget=forms.PasswordInput(
                                    attrs={
                                        'placeholder': 'Repetir contraseña'
                                    }
                                ))

    class Meta:
        model = User
        fields = (
            'username',
            'email',
            'nombre',
            'apellido',
            'avatar',
        )

    def clean_password2(self):
        if self.cleaned_data['password'] != self.cleaned_data['password2']:
            raise forms.ValidationError('Las contraseñas son diferentes')
        if not re.match(r"^[\w\d]{5,}$", self.cleaned_data['password']):
            raise forms.ValidationError('La contrseña esta mal escrita (mínimo 5 letras o números)')


class UserLoginForm(forms.Form):
    username = forms.CharField(label='Usuario',
                               required=True,
                               widget=forms.TextInput(
                                   attrs={
                                       'placeholder': 'Nombre de usuario'
                                   }
                               ))
    password = forms.CharField(label='Contraseña',
                               required=True,
                               widget=forms.PasswordInput(
                                   attrs={
                                       'placeholder': 'Contraseña'
                                   }
                               ))

    def clean(self):
        cleaned_data = super(UserLoginForm, self).clean()
        username = self.cleaned_data['username']
        password = self.cleaned_data['password']
        if not authenticate(username=username, password=password):
            self.add_error('password', 'Datos incorrectos')

        return self.cleaned_data


class VerificationForm(forms.Form):
    codregistro = forms.CharField(label='', required=True)

    def __init__(self, pk, *args, **kwargs):
        self.id_user = pk
        super(VerificationForm, self).__init__(*args, **kwargs)

    def clean_codregistro(self):
        codigo = self.cleaned_data['codregistro']
        print(codigo)
        if re.match(r"^[A-Z0-9]{6}$", codigo):
            activo = User.objects.cod_validation(self.id_user,
                                                 codigo)
            if not activo:
                raise forms.ValidationError('El código es incorrecto')
        else:
            raise forms.ValidationError('El código esta mal escrito')

class OrdenForm(forms.ModelForm):

    numTarjeta = forms.CharField(label='Número de tarjeta',
                               required=True,
                               widget=forms.PasswordInput(
                                   attrs={
                                       'placeholder': '16 dígitos de su tarjeta de crédito o débito'
                                   }
                               ))
    cv2 = forms.CharField(label='Código de seguridad',
                               required=True,
                               widget=forms.PasswordInput(
                                   attrs={
                                       'placeholder': '3 dígitos de su tarjeta de crédito o débito'
                                   }
                               ))
    def __init__(self, pk, *args, **kwargs):
        self.id_libro = pk
        super(OrdenForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Orden
        fields = (
            'paqueteria',
            'cantidad',
            'domicilio',
        )
    def clean_cantidad(self):
        libro = Libro.objects.get(pk=self.id_libro)
        print(self.cleaned_data['cantidad'])
        if(self.cleaned_data['cantidad'] > libro.inventario or self.cleaned_data['cantidad'] <= 0 ):
            raise forms.ValidationError('No hay suficiente inventario')
        return self.cleaned_data['cantidad']

    def clean_numTarjeta(self):
        tarjeta = self.cleaned_data['numTarjeta']
        if not re.match(r"^([0-9]{16})$", tarjeta):
            raise forms.ValidationError('El número de tarjeta esta mal escrito')

    def clean_cv2(self):
        num = self.cleaned_data['cv2']
        if not re.match(r"^([0-9]{3})$", num):
            raise forms.ValidationError('El número de seguridad esta mal escrito')

    def clean_domicilio(self):
        print(self.cleaned_data['domicilio'])
        domicilio = self.cleaned_data['domicilio']
        if not re.match(r"^([a-zA-Z]+((\s[a-zA-Z]+)+)?\s#\d+)$", domicilio):
            raise forms.ValidationError('La dirección esta mal escrita (Es la calle #número casa)')
        return self.cleaned_data['domicilio']