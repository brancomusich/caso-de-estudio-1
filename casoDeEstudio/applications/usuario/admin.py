from django.contrib import admin

# Register your models here.
from .models import User, Orden

admin.site.register(User)

admin.site.register(Orden)