from django.db import models

from django.contrib.auth.models import BaseUserManager

from django.db.models import Avg, Sum, Count
from django.db.models.functions import Lower

class UserManager(BaseUserManager, models.Manager):
    def _create_user(self, username, email, password, is_staff, is_superuser, is_active, **extra_fields):
        user = self.model(
            username = username,
            email = email,
            is_staff = is_staff,
            is_superuser = is_superuser,
            is_active = is_active,
            **extra_fields,
        )
        user.set_password(password)
        #el using sirve para especificar que bd usar
        user.save(using=self.db)
        return user
    def create_user(self, username, email, password=None, **extra_fields):
        return self._create_user(username, email, password,False, False, False, **extra_fields)

    def create_superuser(self, username, email, password=None, **extra_fields):
        return self._create_user(username,email,password,True,True,True,**extra_fields)

    def cod_validation(self, id_user, codRegistro):
        if self.filter(id=id_user, codregistro = codRegistro).exists():
            return True
        else:
            return False
class OrdenManager(models.Manager):

    def librosPromedioEdad(self):
        resultado = self.filter(
            libro__id = '1'
        ).aggregate(
            promedioEdad = Avg('lector__edad'),
            sumaEdad= Sum('lector__edad')
        )
        return resultado

    def numLibrosPrestados(self):
        resultado = self.values(
            'libro'
            #,'lector'
        ).annotate(
            numPrestados = Count('libro'),
            titulo=Lower('libro__titulo'),
        )

        for r in resultado:
            print(r, r['numPrestados'])
        return resultado

    def ordenes(self, id_usuario):
        resultado = self.filter(
            usuario__id = id_usuario
        )
        return resultado