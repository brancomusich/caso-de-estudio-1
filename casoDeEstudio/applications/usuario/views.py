from datetime import date, timedelta


from django.shortcuts import render

# Create your views here.
from django.core.mail import send_mail
from django.views.generic import (
    FormView, View, ListView
)
from django.urls import reverse_lazy, reverse
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect
from django.contrib.auth.mixins import LoginRequiredMixin

# Create your views here.
from .forms import UserRegisterForm, UserLoginForm, VerificationForm, OrdenForm
from .functions import code_generator

from .models import User, Orden
from ..libro.models import Libro


class UserRegisterView(FormView):
    template_name = 'usuario/register.html'
    form_class = UserRegisterForm
    success_url = '/'
    #generamos el código
    def form_valid(self, form):
        codigo = code_generator()
        usuario =User.objects.create_user(
            form.cleaned_data['username'],
            form.cleaned_data['email'],
            form.cleaned_data['password'],
            nombre = form.cleaned_data['nombre'],
            apellido = form.cleaned_data['apellido'],
            codregistro=codigo,
            avatar = form.cleaned_data['avatar'],
        )
        #envir el código al email del user
        asunto = 'Confirmación de email'
        mensaje = 'Código de verificación ' + codigo
        email_remitente = 'branco.musich@alumnos.udg.mx'

        send_mail(asunto, mensaje, email_remitente, [form.cleaned_data['email'],])
        #redirigir a pantalla de validación
        return HttpResponseRedirect(
            reverse(
                'users_app:user-verification',
                kwargs={'pk': usuario.id}
            )
        )

class UserLoginView(FormView):
    template_name = 'usuario/login.html'
    form_class = UserLoginForm
    success_url = reverse_lazy('home_app:inicio')

    def form_valid(self, form):
        user = authenticate(
            username = form.cleaned_data['username'],
            password = form.cleaned_data['password']
        )
        login(self.request, user)
        return super(UserLoginView, self).form_valid(form)

class UserLogoutView(View):
    def get(self, request, *args, **kwargs):
        logout(request)
        return HttpResponseRedirect(reverse(
            'users_app:user-login'
        ))



class CodeVerificationView(FormView):
    template_name = 'usuario/verification.html'
    form_class = VerificationForm
    success_url = reverse_lazy('users_app:user-login')


    def get_form_kwargs(self):
        kwargs = super(CodeVerificationView, self).get_form_kwargs()
        kwargs.update({
            'pk' : self.kwargs['pk'],
        })
        return kwargs

    def form_valid(self, form):
        User.objects.filter(
            id=self.kwargs['pk']
        ).update(
            is_active = True
        )
        return super(CodeVerificationView, self).form_valid(form)

class OrdenView(LoginRequiredMixin, FormView):
    template_name = 'usuario/compra.html'
    login_url = reverse_lazy('users_app:user-login')
    form_class = OrdenForm
    success_url = '/'

    def get_form_kwargs(self):
        kwargs = super(OrdenView, self).get_form_kwargs()
        kwargs.update({
            'pk' : self.kwargs['pk'],
        })
        return kwargs

    def form_valid(self, form):
        libroC= Libro.objects.get(pk = self.kwargs['pk'])
        orden = Orden(
            usuario=self.request.user,
            libro= libroC,
            paqueteria=form.cleaned_data['paqueteria'],
            fecha_venta=date.today(),
            fecha_entrega=date.today() + timedelta(days=3),
            cantidad = form.cleaned_data['cantidad'],
            #total = form.cleaned_data['cantidad'] * libroC.precio + form.cleaned_data['paqueteria'].costo,
            domicilio = form.cleaned_data['domicilio'],
        )
        orden.save()
        #lo comentamos porque con el save() de el models se estaba realizando doble el proceso
        #libro = libroC
        #libro.inventario -= form.cleaned_data['cantidad']
        #libro.save()

        return super(OrdenView, self).form_valid(form)

class OrdenesView(LoginRequiredMixin, ListView):
    template_name = 'usuario/ordenes.html'
    context_object_name = 'ordenes'
    login_url = reverse_lazy('users_app:user-login')

    def get_queryset(self):
        usuario = self.request.user.id
        return Orden.objects.ordenes(usuario)