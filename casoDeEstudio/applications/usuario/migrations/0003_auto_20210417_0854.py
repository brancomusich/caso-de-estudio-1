# Generated by Django 3.1.7 on 2021-04-17 13:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('usuario', '0002_orden'),
    ]

    operations = [
        migrations.AlterField(
            model_name='orden',
            name='total',
            field=models.PositiveIntegerField(blank=True, null=True, verbose_name='Total'),
        ),
    ]
