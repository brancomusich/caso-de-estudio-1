from django.contrib import admin
from django.urls import path

from . import views

app_name = 'users_app'

urlpatterns = [
    path('register/', views.UserRegisterView.as_view(), name = 'user-register'),
    path('login/', views.UserLoginView.as_view(), name = 'user-login'),
    path('logout/', views.UserLogoutView.as_view(), name = 'user-logout'),
    path('user-verification/<pk>/', views.CodeVerificationView.as_view(), name = 'user-verification'),
    path('compra/<pk>/', views.OrdenView.as_view(), name = 'user-compra'),
    path('lista-ordenes/', views.OrdenesView.as_view(), name = 'user-ordenes'),
]
